# pages

#### 介绍
日常工作生活用到的工具集，仅使用静态页面，部分数据从服务器请求。

#### 软件架构
软件架构说明
html、js、css


#### 安装教程

无需安装，浏览器打开html页面即可。
默认使用dev分支。

#### 使用说明

1.  index.html针对电脑端访问，手机端访问时自动跳转到mobileDevtools.html

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


在线使用
 [http://zcjlq.gitee.io/](http://zcjlq.gitee.io/)
